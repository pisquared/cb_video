#!/bin/bash

set -e

NODE_VER="12.18.2"

sudo apt-get install -y build-essential libxi-dev libglu1-mesa-dev libglew-dev pkg-config libcairo2-dev libpango1.0-dev libjpeg-dev libgif-dev librsvg2-dev
wget "https://nodejs.org/dist/v${NODE_VER}/node-v${NODE_VER}-linux-x64.tar.xz"
tar xvfP "node-v${NODE_VER}-linux-x64.tar.xz"
rm "node-v${NODE_VER}-linux-x64.tar.xz"
mv "node-v${NODE_VER}-linux-x64" node_src
ln -s node_src/bin/npm

./npm i editly headless-gl

git clone https://github.com/stackgl/headless-gl
cd headless-gl
git submodule init
git submodule update
../npm install
../npm run rebuild
cd ..
cp headless-gl/build/Release/webgl.node node_modules/gl/build/Release/webgl.node
rm -rf headless-gl

ln -s node_modules/.bin/editly
rm npm package-lock.json

mkdir -p stellarium
cp templates/cb.ssc ~/.stellarium/scripts/
ln -s ~/.stellarium/scripts/cb.ssc ./stellarium/cb.ssc
