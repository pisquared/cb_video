# resize image
convert cb_logo.png -resize 500x423 cb_logo_r.png

# padding / extend with background
convert cb_logo_r.png -background none -gravity center -extent 1920x1080 cb_logo_hd.png

# quick render
./editly --fast example.json